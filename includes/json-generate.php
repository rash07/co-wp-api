<?php

// JSON ENVIADO A LA SUNAT
add_action( 'add_meta_boxes', 'erplugin_response_add_meta_boxes' );
if ( ! function_exists( 'erplugin_response_add_meta_boxes' ) )
{
    function erplugin_response_add_meta_boxes()
    {
        add_meta_box( 'erplugin_response_json', 'JSON Generado', 'erplugin_response_json_api', 'shop_order', 'side', 'core' );
    }
}
if ( ! function_exists( 'erplugin_response_json_api' ) )
{
    function erplugin_response_json_api()
    {
        global $post;

        $order = wc_get_order( $post->ID );

        $resolution_number = get_option('facturaloperu_api_resolution');
        $send_email = get_option('facturaloperu_api_config_send_email');

        // DATA CUSTOMER
        $customer_first_name = get_post_meta( $post->ID, '_billing_first_name', true );
        $customer_last_name  = get_post_meta( $post->ID, '_billing_last_name', true );
        $customer_email      = get_post_meta( $post->ID, '_billing_email', true );
        $customer_company    = get_post_meta( $post->ID, '_billing_company', true );
        $customer_address    = get_post_meta( $post->ID, '_billing_address_1', true );
        $customer_phone      = get_post_meta( $post->ID, '_billing_phone', true );
        $customer_city       = get_post_meta( $post->ID, '_billing_city', true );
        $customer_state      = get_post_meta( $post->ID, '_billing_state', true );
        $customer_postcode   = get_post_meta( $post->ID, '_billing_postcode', true );
        $customer_number     = get_post_meta( $post->ID, '_billing_nif', true );
        $customer_type_doc   = get_post_meta( $post->ID, 'type_document_identification', true );
        $customer_type_organization   = get_post_meta( $post->ID, 'type_organization', true );
        $customer_type_regime   = get_post_meta( $post->ID, 'type_regime', true );
        $merchant_registration = get_post_meta( $post->ID, 'merchant_registration', true );

        $items = [];
        foreach ($order->get_items() as $item_id => $item_data) {
            // Get an instance of corresponding the WC_Product object
            $product = $item_data->get_product();
            $product_name = $product->get_name(); // Get the product name
            $item_quantity = $item_data->get_quantity(); // Get the item quantity
            $item_total = $item_data->get_total(); // Get the item line total
            $t = $order->get_line_tax($item_data) + $order->get_line_subtotal($item_data);

            $item = array(
                "unit_measure_id" => 70,
                "invoiced_quantity" => (string)($item_quantity),
                "line_extension_amount" => "1.0",
                "free_of_charge_indicator" => false,
                "allowance_charges" => array(
                    array(
                        "charge_indicator" => false,
                        "allowance_charge_reason" => "DESCUENTOGENERAL",
                        "amount" => "0.00",
                        "base_amount" => (string)$order->get_line_subtotal($item_data)
                    )
                ),
                "tax_totals" => array(
                    array(
                        "tax_id" => 1,
                        "tax_amount" => (string)($order->get_line_tax($item_data)),
                        "taxable_amount" => (string)($order->get_line_subtotal($item_data)),
                        "percent" => "19.0"
                    )
                ),
                "description" => $product_name,
                "code" => "2",
                "type_item_identification_id" => 4,
                "price_amount" => (string)($order->get_line_subtotal($item_data)),
                "base_quantity" => (string)($item_quantity)
            );

            $items[] = array_merge($item);
        }

        $array = array(
            "number" => 990001002,
            "type_document_id" => 1,
            "resolution_number" => $resolution_number,
            "date" => date("Y-m-d", strtotime($order->get_date_created())),
            "time" => date("h:i:s", strtotime($order->get_date_created())),
            "sendmail" => filter_var($send_email, FILTER_VALIDATE_BOOLEAN),
            "customer" => array(
                "identification_number" => intval($customer_number),
                "dv" => 1, //crear nuevo campo| depende de tipo organizacion2
                "name" => $customer_company,
                "phone" => intval($customer_phone),
                "address" => $customer_address,
                "email" => $customer_email != '' ? $customer_email : '',
                "merchant_registration" => $merchant_registration,
                "type_document_identification_id" => intval($customer_type_doc),
                "type_organization_id" => intval($customer_type_organization), // 1juridica 2natural
                "municipality_id" => $customer_postcode != '' ? intval($customer_postcode)  : 822,
                "type_regime_id" => intval($customer_type_regime)
            ),
            "payment_form" => array(
                "payment_form_id" => 2,
                "payment_method_id" => 30,
                "payment_due_date" => date("Y-m-d", strtotime($order->get_date_created()."+ 1 month")),
                "duration_measure" => "30"
            ),
            "allowance_charges" => array(
                array(
                    "discount_id" => 1,
                    "charge_indicator" => false,
                    "allowance_charge_reason" => "DESCUENTO GENERAL",
                    "amount" => "0.00",
                    "base_amount" => (string)($order->total - $order->total_tax)
                )
            ),
            "legal_monetary_totals" => array(
                "line_extension_amount" => (string)($order->total - $order->total_tax),
                "tax_exclusive_amount" => (string)($order->total - $order->total_tax),
                "tax_inclusive_amount" => $order->total,
                "allowance_total_amount" => "0.00",
                "charge_total_amount" => "0.00",
                "payable_amount" => $order->total
            ),
            "tax_totals" =>
            array(
                array(
                    "tax_id" => 1,
                    "tax_amount" => $order->total_tax,
                    "percent" => "19",
                    "taxable_amount" => (string)($order->total - $order->total_tax)
                )
            ),
            "invoice_lines" => $items
        );

        $erjsonencode = json_encode($array, JSON_PRETTY_PRINT);


        global $post;
        $order = wc_get_order( $post->ID );
        $order_status = $order->get_status();

        if ('auto-draft' != $order_status) {
            echo '<textarea style="width:100%;min-height:250px;">'.$erjsonencode.'</textarea>';
        } else {
            echo 'Se generará al guardar';
        }

    };

}

