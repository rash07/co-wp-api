<?php

// funcion que detecta cuando el estatus es completed
//function mysite_woocommerce_payment_complete( $order_id ) {
    //$order = wc_get_order(  $order_id );

    // The text for the note
    //$note = __("This is my note's text…");

    // Add the note
    //$order->add_order_note( $note );

    // Save the data
    //$order->save();
//}
//add_action( 'woocommerce_order_status_completed', 'mysite_woocommerce_payment_complete' );


add_action( 'add_meta_boxes', 'ep_add_meta_boxes' );
if ( ! function_exists( 'ep_add_meta_boxes' ) )
{
    function ep_add_meta_boxes()
    {
        add_meta_box( 'ep_sunat_fields', 'Factura Enviada', 'ep_add_sunat_fields', 'shop_order');
    }
}

// agrego contenido al box
if ( ! function_exists( 'ep_add_sunat_fields' ) )
{
    function ep_add_sunat_fields()
    {
        global $post;

        // CONSULTO EL DATO DEL CAMPO "SI SE HA ENVIADO LA FACTURA"
        $meta_field_data = get_post_meta( $post->ID, '_send_invoice', true ) ? get_post_meta( $post->ID, '_send_invoice', true ) : '';
        // CONSULTO EL DATO ALMACENADO UNA RESPUESTA DE LA API
        $meta_field_response = get_post_meta( $post->ID, '_ep_sunat_api_response', true ) ? get_post_meta( $post->ID, '_ep_sunat_api_response', true ) : '';
        // CONSULTO LOS DATOS DE LA ORDEN
        $pedido = get_post_meta( $post->ID, '', true );
        // print_r('<pre>'.json_encode($pedido, JSON_PRETTY_PRINT).'</pre>');
        $order = wc_get_order( $post->ID );
        // CONSULTO EL ESTATUS ACTUAL
        $order_status = $order->get_status();

        //VALIDO SI SE HA ENVIADO FACTURA
        $checked = '';
        if ('on' == $meta_field_data) {
            $checked = 'checked="checked"';
        }

        // -- si el status es completed
        if ('completed' == $order_status) {
            $checked = 'checked="checked"';
        }

        // SI YA SE HA ALMACENADO UNA RESPUESTA DE LA API
        if (true == $meta_field_response) {
            //CONSULTO CADA CAMPO QUE SE ALMACENA CUANDO SE GUARDA UNA RESPUESTA DE LA API
            // $meta_field_api_json_response = get_post_meta( $post->ID, '_ep_sunat_api_json_response', true ) ? get_post_meta( $post->ID, '_ep_sunat_api_json_response', true ) : '';
            $meta_field_api_xml = get_post_meta( $post->ID, '_ep_sunat_api_xml', true ) ? get_post_meta( $post->ID, '_ep_sunat_api_xml', true ) : '';
            // $meta_field_api_pdf = get_post_meta( $post->ID, '_ep_sunat_api_pdf', true ) ? get_post_meta( $post->ID, '_ep_sunat_api_pdf', true ) : '';
            // $meta_field_api_cdr = get_post_meta( $post->ID, '_ep_sunat_api_cdr', true ) ? get_post_meta( $post->ID, '_ep_sunat_api_cdr', true ) : '';
            // MUESTRO LOS VALORES DE LA RESPUESTA DE LA API
            echo '
            <table style="margin-bottom: 20px;"><tbody>
                <tr>
                    <td><label style="">Respuesta DIAN</label></td>
                </tr>
                <tr>
                    <td><textarea style="min-width:500px;min-height:300px;">'.$meta_field_api_xml.'</textarea></td>
                </tr>
            </tbody></table>';
        } else {

            // SI LA CASILLA ESTA ACTIVA
            if ('' != $checked) {
                // CARGO EL ARRAY CON LOS DATOS DE LA ORDEN (JSON VALORES ESTATICOS)

                $resolution_number = get_option('facturaloperu_api_resolution');
                $send_email = get_option('facturaloperu_api_config_send_email');

                // DATA CUSTOMER
                $customer_first_name = get_post_meta( $post->ID, '_billing_first_name', true );
                $customer_last_name  = get_post_meta( $post->ID, '_billing_last_name', true );
                $customer_email      = get_post_meta( $post->ID, '_billing_email', true );
                $customer_company    = get_post_meta( $post->ID, '_billing_company', true );
                $customer_address    = get_post_meta( $post->ID, '_billing_address_1', true );
                $customer_phone      = get_post_meta( $post->ID, '_billing_phone', true );
                $customer_city       = get_post_meta( $post->ID, '_billing_city', true );
                $customer_state      = get_post_meta( $post->ID, '_billing_state', true );
                $customer_postcode   = get_post_meta( $post->ID, '_billing_postcode', true );
                $customer_number     = get_post_meta( $post->ID, '_billing_nif', true );
                $customer_type_doc   = get_post_meta( $post->ID, 'type_document_identification', true );
                $customer_type_organization   = get_post_meta( $post->ID, 'type_organization', true );
                $customer_type_regime   = get_post_meta( $post->ID, 'type_regime', true );
                $merchant_registration = get_post_meta( $post->ID, 'merchant_registration', true );

                // RECORRO LOS ITEMS
                $items = [];
                foreach ($order->get_items() as $item_id => $item_data) {
                    $product = $item_data->get_product();
                    $product_name = $product->get_name();
                    $item_quantity = $item_data->get_quantity();
                    $item_total = $item_data->get_total();
                    $t = $order->get_line_tax($item_data) + $order->get_line_subtotal($item_data);

                    // co--------------------------------------------
                    $item = array(
                        "unit_measure_id" => 70,
                        "invoiced_quantity" => (string)($item_quantity),
                        "line_extension_amount" => "1.0",
                        "free_of_charge_indicator" => false,
                        "allowance_charges" => array(
                            array(
                                "charge_indicator" => false,
                                "allowance_charge_reason" => "DESCUENTOGENERAL",
                                "amount" => "0.00",
                                "base_amount" => (string)($order->get_line_subtotal($item_data))
                            )
                        ),
                        "tax_totals" => array(
                            array(
                                "tax_id" => 1,
                                "tax_amount" => (string)($order->get_line_tax($item_data)),
                                "taxable_amount" => (string)($order->get_line_subtotal($item_data)),
                                "percent" => "19.0"
                            )
                        ),
                        "description" => $product_name,
                        "code" => "2",
                        "type_item_identification_id" => 4,
                        "price_amount" => $order->get_line_subtotal($item_data),
                        "base_quantity" => $item_quantity
                    );
                    $items[] = array_merge($item);
                }

                $array = array(
                    "number" => 990001002,
                    "type_document_id" => 1,
                    "resolution_number" => $resolution_number,
                    "date" => date("Y-m-d", strtotime($order->get_date_created())),
                    "time" => date("h:i:s", strtotime($order->get_date_created())),
                    "sendmail" => filter_var($send_email, FILTER_VALIDATE_BOOLEAN),
                    "customer" => array(
                        "identification_number" => intval($customer_number),
                        "dv" => 1, //crear nuevo campo| depende de tipo organizacion2
                        "name" => $customer_company,
                        "phone" => $customer_phone,
                        "address" => $customer_address,
                        "email" => $customer_email != '' ? $customer_email : '',
                        "merchant_registration" => $merchant_registration,
                        "type_document_identification_id" => intval($customer_type_doc),
                        "type_organization_id" => intval($customer_type_organization), // 1juridica 2natural
                        "municipality_id" => $customer_postcode != '' ? intval($customer_postcode)  : 822,
                        "type_regime_id" => intval($customer_type_regime)
                    ),
                    "payment_form" => array(
                        "payment_form_id" => 2,
                        "payment_method_id" => 30,
                        "payment_due_date" => date("Y-m-d", strtotime($order->get_date_created()."+ 1 month")),
                        "duration_measure" => "30"
                    ),
                    "allowance_charges" => array(
                        array(
                            "discount_id" => 1,
                            "charge_indicator" => false,
                            "allowance_charge_reason" => "DESCUENTO GENERAL",
                            "amount" => "0.00",
                            "base_amount" => (string)($order->total - $order->total_tax)
                        )
                    ),
                    "legal_monetary_totals" => array(
                        "line_extension_amount" => (string)($order->total - $order->total_tax),
                        "tax_exclusive_amount" => (string)($order->total - $order->total_tax),
                        "tax_inclusive_amount" => $order->total,
                        "allowance_total_amount" => "0.00",
                        "charge_total_amount" => "0.00",
                        "payable_amount" => $order->total
                    ),
                    "tax_totals" =>
                    array(
                        array(
                            "tax_id" => 1,
                            "tax_amount" => $order->total_tax,
                            "percent" => "19",
                            "taxable_amount" => (string)($order->total - $order->total_tax)
                        )
                    ),
                    "invoice_lines" => $items
                );

                // codifico el array a json
                $body = wp_json_encode($array);
                // ENVIO A LA API
                $api_url = get_option('facturaloperu_api_config_url');
                $software_id = get_option('facturaloperu_api_software_id');
                $api_token = 'Bearer '.get_option('facturaloperu_api_config_token');
                $scheme = parse_url($api_url, PHP_URL_SCHEME) != '' ? parse_url($api_url, PHP_URL_SCHEME) : 'http';
                $service_url = $scheme.'://'.parse_url($api_url, PHP_URL_HOST).'/api/ubl2.1/invoice/'.$software_id;

                $response = wp_remote_post( $service_url, array(
                    'method' => 'POST',
                    'headers' => array(
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'timeout' => 15,
                        'Authorization' => $api_token
                    ),
                    'sslverify' => false,
                    'body' => $body,
                ));

                if ( is_wp_error( $response ) ) {
                   $error_message = $response->get_error_message();
                   echo "Something went wrong: $error_message";
                } else {
                    $body = wp_remote_retrieve_body( $response );
                    $data = json_decode($body);
                    // SI EXISTE EL ARRAY DATA EN LA RESPUESTA
                    if ($data) {
                        // CREO UN ARRAY CON LOS DATOS A GUARDAR
                        // var_dump($data);
                        // echo '<input type="hidden" name="ep_sunat_api_json_response" value="'.$body.'">';
                        // $api_response = [
                        //     'api_number' => $data->data->number,
                        //     'api_xml'    => $data->links->xml,
                        //     'api_pdf'    => $data->links->pdf,
                        //     'api_cdr'    => $data->links->cdr,
                        //     'api_json'   => $erjsonencode,
                        // ];

                        // echo '<input type="hidden" name="ep_sunat_api_number" value="'.$api_response['api_number'].'">';
                        echo '<textarea type="hidden"  name="ep_sunat_api_xml"'.$body.'></textarea>';
                        // echo '<input type="hidden" name="ep_sunat_api_pdf" value="'.$api_response['api_pdf'].'">';
                        // echo '<input type="hidden" name="ep_sunat_api_cdr" value="'.$api_response['api_cdr'].'">';
                        // echo '<input type="hidden" name="ep_sunat_api_json" value="'.$api_response['api_json'].'">';
                        echo '<input type="hidden" name="ep_sunat_meta_fields_api_nonce" value="' . wp_create_nonce() . '">';

                        // envio / actualizo la pagina con los valores ya incertados en los inputs
                        echo   "<script>
                                window.onload = function(){
                                    document.post.submit();
                                }
                                </script>";

                    } else {
                        print_r($data);
                    }

                }


            }

            // Pending payment – Order received, no payment initiated. Awaiting payment (unpaid).
            // Failed – Payment failed or was declined (unpaid). Note that this status may not show immediately and instead show as Pending until verified (e.g., PayPal).
            // Processing – Payment received (paid) and stock has been reduced; order is awaiting fulfillment. All product orders require processing, except those that only contain products which are both Virtual and Downloadable.
            // Completed – Order fulfilled and complete – requires no further action.
            // On-Hold – Awaiting payment – stock is reduced, but you need to confirm payment.
            // Cancelled – Cancelled by an admin or the customer – stock is increased, no further action required.
            // Refunded – Refunded by an admin – no further action required.

            if ('completed' == $order_status) {
                if ('' == $checked) {
                    echo '<button type="button" onclick="myFunction()" class="button generate-items">Enviar</button>';
                    echo '<script>
                            function myFunction() {
                                document.getElementById("send_invoice").checked = true;
                                document.post.submit();
                            }
                            </script>';
                }
                // datos del post -> print_r(json_encode($erpost))
                echo '<input type="hidden" name="ep_sunat_meta_field_nonce" value="' . wp_create_nonce() . '">';
                echo '<p style="display:none;">
                    <input type="checkbox" name="send_invoice" id="send_invoice" '.$checked.'>
                </p>';
            } else {
                echo "Estará disponible al momento de que el cliente cancele el producto";
            }

        }

        // SI EXISTE UNA RESPUESTA
        if (isset($data)) {
            // MUESTRO LOS DATOS DE LA RESPUESTA
            echo "<br>";
            // echo '
            // <table style="margin-bottom: 20px;"><tbody>
            //     <tr>
            //         <td><label style="">Numero de Factura Sunat</label></td>
            //         <td><input type="hidden" name="ep_sunat_api_number" value="'.$api_response['api_number'].'">'.$api_response['api_number'].'</td>
            //     </tr>
            //     <tr>
            //         <td><label style="">XML</label></td>
            //         <td><a href="'.$api_response['api_xml'].'">'.$api_response['api_xml'].'</a><input type="hidden" name="ep_sunat_api_xml" value="'.$api_response['api_xml'].'"></td>
            //     </tr>
            //     <tr>
            //         <td><label style="">PDF</label></td>
            //         <td><a href="'.$api_response['api_pdf'].'">'.$api_response['api_pdf'].'</a><input type="hidden" name="ep_sunat_api_pdf" value="'.$api_response['api_pdf'].'"></td>
            //     </tr>
            //     <tr>
            //         <td><label style="">CDR</label></td>
            //         <td><a href="'.$api_response['api_cdr'].'">'.$api_response['api_cdr'].'</a><input type="hidden" name="ep_sunat_api_cdr" value="'.$api_response['api_cdr'].'"></td>
            //     </tr>
            //     <input type="hidden" name="ep_sunat_api_json" value="'.$api_response['api_json'].'">
            // </tbody></table>';
            echo '
            <table style="margin-bottom: 20px;"><tbody>
                <tr>
                    <td><label style="">Respuesta DIAN</label></td>
                    <td>
                        <textarea type="hidden" style="min-width:500px;" name="ep_sunat_api_xml">'.$body.'</textarea>
                    </td>
                </tr>

            </tbody></table>';

            echo '<input type="hidden" name="ep_sunat_meta_fields_api_nonce" value="' . wp_create_nonce() . '">';

            echo ' <button type="button" onclick="document.post.submit();" class="button generate-items">Salvar Respuesta</button>';
        }

    }
}

// FUNCION QUE SE EJECUTA AL GUARDAR/ACTUALIZAR UNA ORDEN
add_action( 'save_post', 'ep_save_sunat_field', 10, 1 );
if ( ! function_exists( 'ep_save_sunat_field' ) )
{

    function ep_save_sunat_field( $post_id ) {

        // We need to verify this with the proper authorization (security stuff).

        // CHEQUEO SI EL CAMPO EXISTE (CHECKBOX)
        if ( ! isset( $_POST[ 'ep_sunat_meta_field_nonce' ] ) ) {
            return $post_id;
        }
        $nonce = $_REQUEST[ 'ep_sunat_meta_field_nonce' ];

        //Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce ) ) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        // ACTUALIZO EL VALOR DEL CAMPO
        update_post_meta( $post_id, '_send_invoice', $_POST[ 'send_invoice' ] );
    }
}

// FUNCION QUE SE EJECUTA AL GUARDAR/ACTUALIZAR UNA ORDEN
add_action( 'save_post', 'ep_save_sunat_field_response', 10, 1 );
if ( ! function_exists( 'ep_save_sunat_field_response' ) )
{

    function ep_save_sunat_field_response( $post_id ) {

        // We need to verify this with the proper authorization (security stuff).

        // SI EXISTE EL CAMPO (HIDDEN CON DATOS DE RESPUESTA DE API)
        if ( ! isset( $_POST[ 'ep_sunat_meta_fields_api_nonce' ] ) ) {
            return $post_id;
        }
        $nonce = $_REQUEST[ 'ep_sunat_meta_fields_api_nonce' ];

        //Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce ) ) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        // GUARDO LOS CAMPOS
        update_post_meta( $post_id, '_ep_sunat_api_response', true );
        // update_post_meta( $post_id, '_ep_sunat_api_json_response', $_POST[ '_ep_sunat_api_json_response' ] );
        // update_post_meta( $post_id, '_ep_sunat_api_number', $_POST[ 'ep_sunat_api_number' ] );
        update_post_meta( $post_id, '_ep_sunat_api_xml', $_POST[ 'ep_sunat_api_xml' ] );
        // update_post_meta( $post_id, '_ep_sunat_api_pdf', $_POST[ 'ep_sunat_api_pdf' ] );
        // update_post_meta( $post_id, '_ep_sunat_api_cdr', $_POST[ 'ep_sunat_api_cdr' ] );
        // update_post_meta( $post_id, '_ep_sunat_api_json', $_POST[ 'ep_sunat_api_json' ] );
    }
}

add_filter( 'http_request_timeout', 'wp9838c_timeout_extend' );

function wp9838c_timeout_extend( $time )
{
    // Default timeout is 5
    return 20;
}
