<?php
function add_scripts_api_queries() {
    wp_enqueue_script(
        'api-script',
        plugin_dir_url( __FILE__ ) . '../../js/api-queries.js',
        array( 'jquery' )
    );

    wp_localize_script('api-script', 'api_script_object', array(
        'ajaxurl' => admin_url('admin-ajax.php')
    ));
}

add_action('admin_enqueue_scripts', 'add_scripts_api_queries');
add_action( 'wp_ajax_api_service_config_company', 'api_service_config_company' );
add_action( 'wp_ajax_nopriv_api_service_config_company', 'api_service_config_company' );

function api_service_config_company() {
    $type = $_POST['type'];

    $dv = get_option('facturaloperu_api_config_dv');
    $api_url = get_option('facturaloperu_api_config_url');
    $document = get_option('facturaloperu_api_config_document');

    $scheme = parse_url($api_url, PHP_URL_SCHEME) != '' ? parse_url($api_url, PHP_URL_SCHEME) : 'http';
    $service_url = $scheme.'://'.parse_url($api_url, PHP_URL_HOST).'/api/ubl2.1/config/'.$document.'/'.$dv;

    $body = wp_json_encode([
        "type_document_identification_id" => get_option('facturaloperu_api_config_document_type'),
        "type_organization_id" => get_option('facturaloperu_api_config_organization_type'),
        "type_regime_id" => get_option('facturaloperu_api_config_regime_type'),
        "type_liability_id" => get_option('facturaloperu_api_config_liability_type'),
        "business_name" => get_option('facturaloperu_api_config_business_name'),
        "merchant_registration" => get_option('facturaloperu_api_config_merchant_registration'),
        "municipality_id" => get_option('facturaloperu_api_config_municipality'),
        "address" => get_option('facturaloperu_api_config_business_address'),
        "phone" => get_option('facturaloperu_api_config_business_phone'),
        "email" => get_option('facturaloperu_api_config_business_email')
    ]);

    // ENVIO A LA API
    $response = wp_remote_post( $service_url, array(
        'method' => 'POST',
        'headers' => array(
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ),
        'sslverify' => false,
        'body' => $body
    ));

    if ( is_wp_error( $response ) ) {
        $error_message = $response->get_error_message();
        echo "Something went wrong: $error_message";
    } else {
        echo $response;
        $body = wp_remote_retrieve_body( $response );
        echo $body;
    }

    wp_die();
}